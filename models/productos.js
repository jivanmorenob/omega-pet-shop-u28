const { model, Schema } = require("mongoose");

const ProductosSchema = new Schema({
  idProducto: { type: Number, required: true, unique: true },
  nombre: { type: String, required: true },
  precio: { type: Number, required: true },
  cantidad: { type: Number, required: true },
});

module.exports = model("Productos", ProductosSchema);