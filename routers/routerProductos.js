const { Router } = require ('express');
const router = Router(router.get('/prueba',controllerUsuarios));
var controllerProductos=require('../controllers/ControllerProductos');

//export default router;

router.post('/crear',controllerProductos.saveProductos);
router.get('/buscar/:id',controllerProductos.buscarData);
router.get('/buscarall/:id?',controllerProductos.listarAllData);
router.delete('/borrar/:id',controllerProductos.deleteProductos);
router.put('/actualizar/:id',controllerProductos.updateProductos);


module.exports = router;