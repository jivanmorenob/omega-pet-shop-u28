const { Router } = require("express");
const router = Router();
// export default router;

var controllerUsuarios=require('../controllers/controllerUsuarios');

router.get('/prueba',controllerUsuarios.saludo);

router.post('/crear',controllerUsuarios.saveUsuario);

router.get('/buscar/:id',controllerUsuarios.buscarData);
router.get('/buscarall/:id?',controllerUsuarios.listarAllData);

router.delete('/borrar/:id',controllerUsuarios.deleteUsuario);
router.put('/actualizar/:id',controllerUsuarios.updateUsuario);


module.exports = router;