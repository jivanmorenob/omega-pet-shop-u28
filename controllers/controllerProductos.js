const Productos = require("../models/productos");

//CREAR NUEVO PRODUCTO
function saveProductos(req, res) {
    var myProductos = new Productos(req.body);
    myProductos.save((err, result) => {
        res.status(200).send({ message: 'guardando un producto' });
    });
}

//BUSCAR POR ID
function buscarData(req, res) {
    var idProductos = req.params, id;
    Productos.findById(idProductos).exec((err, result) => {
        if (err) {
            res.status(500).send({ message: 'Error al momento de ejecutar la solicitud' });
        } else {
            if (!result) {
                res.status(404).send({ message: 'El registro a buscar no se encuentra disponible' });
            } else {
                res.status(200).send({ result });
            }
        }
    });

}
//LISTAR POR ID
function listarAllData(req, res) {
    var idProductos = req.params.idb;
    if (!idProductos) {
        var result = Productos.find({}).sort('nombre');
    } else {
        var result = Productos.find({ _id: idProductos }).sort('nombre');
    }
    result.exec(function (err, result) {
        if (err) {
            res.status(500).send({ message: 'Error al momento de ejecutar la solicitud' });
        } else {
            if (!result) {
                res.status(404).send({ message: 'El registro a buscar no se encuentra disponible' })
                    ;
            } else {
                res.status(200).send({ result });
            }
        }
    })
}

//ACTUALIZAR
const updateProductos = async (req, res) => {
    try {
        const id = req.params.id;
        const Productos = req.body;
        await Productos.findByIdAndUpdate(id, Productos);
        res.send("Producto actualizado correctamente");
    } catch (error) {
        console.error(error);
    }
}

//BORRAR
function deleteProductos(req, res) {
    var idProductos = req.params.id;
    Productos.findByIdAndRemove(idProductos, function (err, productos) {
        if (err) {
            return res.json(500, {
                message: 'No hemos encontrado el producto'
            })
        }
        return res.json(productos)
    });
}
module.exports = {
    saveProductos,
    buscarData,
    listarAllData,
    updateProductos,
    deleteProductos
}